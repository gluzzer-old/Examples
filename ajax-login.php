<?php
session_start();
require_once '../../private/settings.inc';
require_once '../../private/c_user.php';

$user = @$_POST['username'];
$pass = @$_POST['password'];

$response = array();

if($user == null) {
    $errors = 1;
    $response['statusmsg'] .= "Please enter your username.\n";
}
if($pass == null) {
    $errors = 1;
    $response['statusmsg'] .= "Please enter your password.\n";
}
if($errors === 1) {
    $response['status'] = 'error';
    echo json_encode($response);
    return;
} else {
    $login = new User;
    $loginstatus = $login->login($user, $pass);

    if($loginstatus === 0) {
        $response['status'] = 'error';
        $response['statusmsg'] = "The system was unable to log you in. Please try again later.\nIf this error presists please inform the site administrator.";
        echo json_encode($response);
        return;
    }
    if($loginstatus === 2) {
        $response['status'] = 'error';
        $response['statusmsg'] = "There was an error. Please try again later.\nIf this error presists please inform the site administrator.";
        echo json_encode($response);
        return;
    }
    if($loginstatus === 1) {
        $response['status'] = 'success';
        echo json_encode($response);
        return;
    }
}